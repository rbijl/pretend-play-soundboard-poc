function createHtmlFormForVoices(voices) {

  document.body.insertAdjacentHTML('beforeend', `
    <form>
    <input type="text" class="txt" value="Hello world"></input>
    <div>
      <label for="rate">Rate</label><input type="range" min="0.5" max="2" value="1" step="0.1" id="rate">
      <div class="rate-value">1</div>
      <div class="clearfix"></div>
    </div>
    <div>
      <label for="pitch">Pitch</label><input type="range" min="0" max="2" value="1" step="0.1" id="pitch">
      <div class="pitch-value">1</div>
      <div class="clearfix"></div>
    </div>
    <select>

    </select>
    <div class="controls">
      <button id="play" type="submit">Play</button>
    </div>
    </form>
`);

  var inputForm = document.querySelector('form');
  var inputTxt = document.querySelector('input.txt');
  var voiceSelect = document.querySelector('select');

  for(var i = 0; i < voices.length; i++) {
    var option = document.createElement('option');
    option.textContent = '(#' + i + ') ' + voices[i].name + ' (' + voices[i].lang + ')';
    option.value = i;
    voiceSelect.appendChild(option);
  }

  inputForm.onsubmit = function(event) {
    event.preventDefault();

    var utterThis = new SpeechSynthesisUtterance(inputTxt.value);
    utterThis.voice = voices[voiceSelect.value];
    utterThis.name = voices[voiceSelect.value].name;
    utterThis.lang = voices[voiceSelect.value].lang;
    window.speechSynthesis.speak(utterThis);
    inputTxt.blur();
  }
}
