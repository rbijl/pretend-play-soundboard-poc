class HtmlButton {
  constructor(button) {
    this._button = button;
    this._element = this.initElement();
    this.updateElement();
  }

  get button() {
    return this._button;
  }

  get element() {
    return this._element;
  }

  initElement() {
    let myInput = document.createElement('input');
    myInput.setAttribute('type', 'image');
    myInput.setAttribute('width', '140');
    myInput.setAttribute('height', '140');
    myInput.setAttribute('alt', this._button.name);
    myInput.setAttribute('src', this._button.image);
    return myInput;
  }

  updateElement() {
    if (this._button.onclick !== null) {
      this._element.classList.add("button");
      this._element.onclick = () => {
        this._element.classList.add("active");
        this._button.onclick(() => { this._element.classList.remove("active"); });
      };
    }
  }
}

class Button {
  constructor(name, image, onclick) {
    if (new.target === Button) {
      throw new TypeError("Cannot construct Button instances directly");
    }
    this._name = name;
    this._image = image;
    this._onclick = onclick;
  }

  get name() {
    return this._name;
  }

  get image() {
    return this._image;
  }

  get onclick() {
    return this._onclick;
  }
}

class PlaceholderButton extends Button {
  constructor() {
    super("placeholder", "images/control-tower.jpg", null);
  }
}

class SpeechButton extends Button {
  constructor(name, image, text, voice) {
    super(name, image, null);
    this._text = text;
    this._voice = voice;
  }

  get text() {
    return this._text;
  }

  get voice() {
    return this._voice;
  }

  onclick(onclick) {
    this._onclick = onclick;
  }
}

class AudioButton extends Button {
  constructor(name, image, onclick) {
    super(name, image, onclick);
  }
}
